/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.labox4;

/**
 *
 * @author informatics
 */
public class Table {

    private char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    private Player player1;
    private Player player2;
    private Player currentPlayer;
    private int turnCount = 0;
    private int row, col;

    public Table(Player player1, Player player2) {
        this.player1 = player1;
        this.player2 = player2;
        this.currentPlayer = player1;

    }

    public char[][] getTable() {
        return table;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public boolean setRowCol(int row, int col) {
        if (table[row - 1][col - 1] == '-') {
            table[row - 1][col - 1] = currentPlayer.getSymbol();
            this.row = row;
            this.col = col;
            return true;
        }
        return false;
    }

    public void switchPlayer() {
        turnCount++;
        if (currentPlayer == player1) {
            currentPlayer = player2;
        } else {
            currentPlayer = player1;
        }
    }

    private boolean checkRow() {
        return table[row - 1][0] != '-' && table[row - 1][0] == table[row - 1][1] && table[row - 1][1] == table[row - 1][2];
    }

    private boolean checkCol() {
        return table[0][col - 1] != '-' && table[0][col - 1] == table[1][col - 1] && table[1][col - 1] == table[2][col - 1];
    }

    private boolean checkTopLeft_to_BottomRight() {
        return table[0][0] == currentPlayer.getSymbol() && table[1][1] == currentPlayer.getSymbol() && table[2][2] == currentPlayer.getSymbol();
    }
    
    private boolean checkTopRight_to_BottomLeft() {
        return table[0][2] == currentPlayer.getSymbol() && table[1][1] == currentPlayer.getSymbol() && table[2][0] == currentPlayer.getSymbol();
    }

    boolean checkDraw() {
        if (turnCount == 8) {
            saveDraw();
            return true;
        }
        return false;
    }

    boolean checkWin() {
        if (checkRow()) {
            saveWin();
            return true;
        } else {
            if (checkCol()) {
                saveWin();
                return true;
            } else {
                if (checkTopLeft_to_BottomRight()) {
                    saveWin();
                    return true;
                } else {
                    if (checkTopRight_to_BottomLeft()) {
                        saveWin();
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private void saveWin() {
        if (player1 == getCurrentPlayer()) {
            player1.win();
            player2.lose();
        } else {
            player2.win();
            player1.lose();
        }
    }

    private void saveDraw() {
        player1.draw();
        player2.draw();
    }

}
