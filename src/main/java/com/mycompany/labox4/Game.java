/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.labox4;

import java.util.Scanner;

/**
 *
 * @author informatics
 */
public class Game {

    private Player player1;
    private Player player2;
    private Table table;
    private char InputPlayAgain = '-';

    public Game() {
        this.player1 = new Player('O');
        this.player2 = new Player('X');
    }

    public void newGame() {
        this.table = new Table(player1, player2);
    }

    public void play() {

        showWelcome();
        newGame();
        while (true) {
            showTable();
            showTurn();
            inputRowCol();
            if (table.checkWin()) {
                showTable();
                showInfo();
                playAgain();
                if (InputPlayAgain == 'N') {
                    break;
                } else {
                    newGame();
                }

            }
            if (table.checkDraw()) {
                showTable();
                showInfo();
                playAgain();
                if (InputPlayAgain == 'N') {
                    break;
                } else {
                    newGame();
                }
            }
            if (InputPlayAgain == 'N') {
                break;
            }
            table.switchPlayer();
        }

    }

    private void playAgain() {
        System.out.print("Do you want to play again?(Y/N): ");
        Scanner sc = new Scanner(System.in);
        InputPlayAgain = sc.next().charAt(0);
    }

    private void showWelcome() {
        System.out.println("welcome to OX");
    }

    private void showTable() {
        char[][] t = table.getTable();
        System.out.println("-------------------");
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print("| " + t[i][j] + " ");
            }
            System.out.print("|");
            System.out.println("");
        }
        System.out.println("-------------------");

    }

    private void showTurn() {
        System.out.print("Player " + table.getCurrentPlayer().getSymbol() + ",");
    }

    private void inputRowCol() {
        Scanner sc = new Scanner(System.in);
        System.out.print(" enter your row, col: ");
        int row = sc.nextInt();
        int col = sc.nextInt();
        table.setRowCol(row, col);
    }

    private void showInfo() {
        System.out.println(player1);
        System.out.println(player2);
    }
}
